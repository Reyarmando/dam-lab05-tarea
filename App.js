import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
  Button,
} from 'react-native';
import LoguinScreen from './components/Loguin';
import OurFlatList from './components/Lista';
import Detail from './components/Detalle';


const Stack = createStackNavigator();



export default class App extends Component {
  
  render() {
    return(
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Loguin" component={LoguinScreen}/>
          <Stack.Screen name="Lista" component={OurFlatList}/>
          <Stack.Screen name="Detalle" component={Detail}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
  },
  imput: {

  }
});