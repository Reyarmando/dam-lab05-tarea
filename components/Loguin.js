import * as React from 'react';
import { Button, View, Image, Text, TextInput } from 'react-native';

export default function LoguinScreen({navigation}) {
    
    return (
      <View>
        <Text> LOGIN </Text>
        <TextInput
            placeholder={'Username'}
            placeholderTextColor={'gray'}
            underlineColorAndroid='white'
  
          />

        <TextInput
            placeholder={'Password'}
            placeholderTextColor={'gray'}
            underlineColorAndroid='white'
            keyboardType='visible-password'
            secureTextEntry={true}
          />
  
  
          <Button
            title="Loguin"
            onPress={ () => navigation.navigate('Lista')}
          />
      </View>
    )
  }
  