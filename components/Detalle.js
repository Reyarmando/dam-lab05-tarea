
import * as React from 'react';
import {View, FlatList, StyleSheet, Text, Image, Button, TouchableOpacity} from 'react-native';





export default function Detail({route, navigation}) {

    const {itemTitle} = route.params;
    const {itemDesc} = route.params;
    const {itemPic} = route.params;
  
    return (
        <View style={{backgroundColor: '#212121', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      
        <Text style={{color: 'white' , fontSize: 40, marginBottom: 20}}>{itemTitle}</Text>
        <Image style={{aspectRatio: 2/3 , width:250 , marginBottom: 20} } source={{uri: itemPic}} />
        <Text style={{color: 'white' , fontSize: 24, marginBottom: 20}}>Descripcion: {itemDesc}</Text>

      <TouchableOpacity onPress = {() => navigation.navigate('Lista')}>
        <View style = {{padding:10, backgroundColor: '#2196f3', alignItems: 'center', justifyContent: 'center', borderRadius: 4}}>
          <Text style = {{fontSize: 20, color: 'white'}}>VOLVER</Text>
        </View>
      </TouchableOpacity>
    </View>
    );
  }