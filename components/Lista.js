import * as React from 'react';
import { View, FlatList, StyleSheet, Text, Button, Image, ImageRequireSource, TouchableOpacity } from 'react-native';
import { BaseRouter } from '@react-navigation/native';


const DATA = [
    {
        "image": "./ferrari.jpg",
        "id": "1",
        "title": "Ferrari 2020 \n deportivo",
    },
    {
        "image": "./ferrari.jpg",
        "id": "1",
        "title": "Audi año 2019 \n deportivo",
        "detalles": "Audi año 2019 \n deportivo",
    },
    {
        "image": "./ferrari.jpg",
        "id": "1",
        "title": "Ford 2020 \n deportivo",
    },
];

var flatListData = [
    {
        "key": "1",
        "name": "Alien: Isolation",
        "imageUrl": "https://store.playstation.com/store/api/chihiro/00_09_000/container/PE/es/999/UP0177-CUSA00363_00-ALIENISOLATIONP4/1580198588000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000",
        "foodDescription": "Conoce el verdadero miedo con Alien: Isolation, un survival horror donde el miedo te persigue y el peligro es mortal."
    },
    {
        "key": "12",
        "name": "Metro Exodus",
        "imageUrl": "https://peru21.pe/resizer/xTq4QxQCoboiDhf0Er4JYNoQoJw=/980x528/smart/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/TOWIQ5UDY5ERZG7MP3GSMKHJDU.jpg",
        "foodDescription": "un épico shooter argumental en primera persona de 4A Games que combina combates mortales y sigilo con exploración y survival horror en uno de los mundos más envolventes jamás creado para un juego."
    },
    {
        "key": "123",
        "name": "Shadow of the Tomb Raider",
        "imageUrl": "https://cdn.mos.cms.futurecdn.net/HQ9Y8xruwDvroajm4Na3Ao.jpg",
        "foodDescription": "Mientras Lara Croft corre para salvar al mundo de un apocalipsis maya, debe convertirse en la Tomb Raider que está destinada a ser"
    },
    {
        "key": "1234",
        "name": "ARK: Survival Evolved",
        "imageUrl": "https://s2.gaming-cdn.com/images/products/904/orig/ark-survival-evolved-cover.jpg",
        "foodDescription": "Como un hombre o una mujer varados desnudos, helados y hambrientos a orillas de una isla misteriosa llamada ARK, debes cazar, cosechar recursos, fabricar"
    },
    {
        "key": "12345",
        "name": "Need for Speed Heat",
        "imageUrl": "https://hardzone.es/app/uploads-hardzone.es/2019/10/NFS-Heat.jpg",
        "foodDescription": "Esta secuela nos llevará a la ciudad de Palm City, un nuevo mundo abierto en el que se desarrollarán todas las nuevas carreras del videojuego."
    },
    {
        "key": "123456",
        "name": "Amnesia: The Dark Descent",
        "imageUrl": "https://steamcdn-a.akamaihd.net/steam/apps/57300/header.jpg?t=1574253555",
        "foodDescription": "Un horror de supervivencia en primera persona. Un juego sobre inmersión, descubrimiento y vivir una pesadilla. Una experiencia que te relajará hasta la médula."
    },
    {
        "key": "1234567",
        "name": "Resident Evil 3 ",
        "imageUrl": "https://steamcdn-a.akamaihd.net/steam/apps/1173690/capsule_616x353.jpg?t=1584637335",
        "foodDescription": "Sigue a los personajes de Jill Valentine y Carlos Oliveira intentando sobrevivir a un apocalipsis zombi en Raccoon City mientras son perseguidos por Nemesis"
    },
    {
        "key": "12345678",
        "name": "Someday You'll Return",
        "imageUrl": "https://s1.gaming-cdn.com/images/products/6378/orig/someday-youll-return-cover.jpg",
        "foodDescription": "Presenta conceptos tan íntimos como es la paternidad, los miedos, las mentiras y la negativa a aceptar el pasado."
    },
    {
        "key": "123456789",
        "name": "Daymare: 1998",
        "imageUrl": "https://thumbnails.pcgamingwiki.com/4/41/Daymare_1998_cover.png/300px-Daymare_1998_cover.png",
        "foodDescription": "Survival horror en tercera persona con mecánicas de supervivencia y enemigos difíciles de matar. "
    },
];



export default function OurFlatList({ navigation }) {
    
    function Item({ title, image, descripcion }) {
   
        return (
            <TouchableOpacity onPress={() => navigation.navigate('Detalle', {
                itemTitle: title,
                itemDesc: descripcion,
                itemPic: image 
            })}
      
            style={styles.item}>
                
                <Image  
                style={{width: 100, height: 100}} source={{ uri: image }}></Image>
                <View style={{paddingLeft: 20}}>
                <Text 
                style={styles.title}>{title}</Text>
                <Text style={{fontSize: 20, paddingHorizontal: 10, paddingRight: 70 }}>
                    {descripcion}</Text>
                
                </View>
                
            </TouchableOpacity>
        );
    }
    return (
        <View style={styles.container}>
            
            <FlatList
            data={flatListData}
            renderItem={({item}) => (
                    <Item title={item.name} image={item.imageUrl} descripcion={item.foodDescription} />

                )}
                keyExtractor={item => item.id}
                />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    },
    item: {
        backgroundColor: '#c60000',
        borderBottomEndRadius: 12,
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        flexDirection: 'row',
        marginBottom: 3
    },
    title: {
        fontSize: 25,
        color: '#A3FF0F',
        borderRadius: 1
    }
});